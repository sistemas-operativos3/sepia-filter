from flask import Flask, request, jsonify
from flask_cors import CORS

import cv2
import numpy as np
from io import BytesIO
from PIL import Image
import base64
import qimage2ndarray as q2n
from PyQt5 import QtCore

app = Flask(__name__)
app.config['DEBUG'] = True
CORS(app)

@app.route('/sepiaFilter', methods=['POST'])
def sepia():
    base64_decoded = base64.b64decode(request.form.get('data'))
    image = Image.open(BytesIO(base64_decoded))

    width, height = image.size
    sepia_intensity = 25

    for w in range(width):
        for h in range(height):
            r, g, b = image.getpixel((w, h))
            gray = (r+g+b)/3
            r = gray + (sepia_intensity * 2)
            g = gray + sepia_intensity
            b = gray - sepia_intensity

            if r > 255:
                r = 255
            if g > 255:
                g = 255
            if b < 0:
                b = 0

            image.putpixel((w, h), (int(r), int(g), int(b)))

    image = np.array(image)

    buffer = QtCore.QBuffer()
    buffer.open(buffer.WriteOnly)
    buffer.seek(0)

    imagenBase64 = q2n.array2qimage(image)

    imagenBase64.save(buffer, 'PNG', quality=100)
    result = bytes(buffer.data().toBase64()).decode()

    return jsonify({
        'data': result
        }), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5002)